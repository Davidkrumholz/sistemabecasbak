﻿using becasPosgrado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using static becasPosgrado.Models.CoordinadorModel;

namespace becasPosgrado.Controllers
{
    [RoutePrefix("api/Coordinador")]
    public class CoordinadorController : ApiController
    {
        public Helpers entidades = new Helpers();

        [Route("setPuntoEquilibrio")]
        [HttpPost]
        public RespuestaCoordinador insertarPuntoEquilibrio(ParametrosNuevo parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinador Respuesta = new RespuestaCoordinador();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Programas programa = entidades.conexion.TP_Programas.FirstOrDefault(r => r.Cve_Programa == parametros.CvePrograma.ToUpper() && r.Periodo == parametros.Periodo && r.Activo == true);
                if(programa != null)
                {
                    Respuesta.Mensaje = "Punto de Equilibrio ya existente";
                    Respuesta.Status = false;
                    Respuesta.Lista = null;
                }
                else
                {
                    try
                    {
                        TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                        if (coordinador != null)
                        {
                            long IdCoordinador = coordinador.Id;
                            try
                            {
                                TP_Programas nuevo = new TP_Programas
                                {
                                    Id_Coordinador = IdCoordinador,
                                    Periodo = parametros.Periodo,
                                    Cve_Programa = parametros.CvePrograma.ToUpper(),
                                    Nombre_Programa = parametros.NombrePrograma,
                                    Punto_Equilibrio = parametros.PuntoEquilibrio,
                                    Numero_Inscritos = parametros.NumeroInscritos,
                                    Activo = true,
                                    Usuario = parametros.Usuario,
                                    Fecha_Crea = Date,
                                    Fecha_Mod = Date
                                };

                                entidades.conexion.TP_Programas.Add(nuevo);
                                if (entidades.conexion.SaveChanges() > 0)
                                {
                                    Respuesta.Mensaje = "Programa insertado con exito";
                                    Respuesta.Status = true;
                                    Respuesta.Lista = nuevo;
                                }
                            }
                            catch (Exception e) {
                                Respuesta.Status = false;
                                Respuesta.Mensaje = "Error: Programa no insertado";
                                Respuesta.Lista = null;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Coordinador no encontrado";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Programa no insertado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getPuntoEquilibrio")]
        [HttpPost]
        public RespuestaPrograma getPuntoEquilibrio(ParametrosPrograma parametros)
        {
            RespuestaPrograma Respuesta = new RespuestaPrograma();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediete && r.Activo == true);
                if(coordinador != null)
                {
                    long IdCoordinador = coordinador.Id;
                    try
                    {
                        List<TP_Programas> data = entidades.conexion.TP_Programas
                    .Where(r => r.Id_Coordinador == IdCoordinador && r.Periodo == parametros.Periodo && r.Activo == true).ToList();

                        if (data.Count() == 0)
                        {
                            Respuesta.Status = false;
                            Respuesta.Mensaje = "Punto de equilibrio no encontrado";
                            Respuesta.Lista = null;
                        }
                        else if (data != null)
                        {
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Punto de equilibrio encontrado";
                            Respuesta.Lista = data;
                        }
                    }
                    catch(Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Punto de equlibrio no encontrado";
                        Respuesta.Lista = null;
                    }
                }
                
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Coordinador no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAllPuntoEquilibrio")]
        [HttpPost]
        public RespuestaPrograma getAllPuntoEquilibrio(ParametrosPrograma parametros)
        {
            RespuestaPrograma Respuesta = new RespuestaPrograma();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediete && r.Activo == true);
                if (coordinador != null)
                {
                    long IdCoordinador = coordinador.Id;
                    try
                    {
                        List<TP_Programas> data = entidades.conexion.TP_Programas.Where(r => r.Id_Coordinador == IdCoordinador && r.Activo == true).ToList();

                        if (data.Count() == 0)
                        {
                            Respuesta.Status = false;
                            Respuesta.Mensaje = "Punto de equilibrio no encontrado";
                            Respuesta.Lista = null;
                        }
                        else if (data != null)
                        {
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Punto de equilibrio encontrado";
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Punto de equlibrio no encontrado";
                        Respuesta.Lista = null;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Coordinador no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updatePuntoEquilibrio")]
        [HttpPost]
        public RespuestaCoordinador updatePuntoEquilibrio(ParametrosCoordinador parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinador Respuesta = new RespuestaCoordinador();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Programas data = entidades.conexion.TP_Programas
                    .FirstOrDefault(r => r.Cve_Programa == parametros.CvePrograma.ToUpper() && r.Periodo == parametros.Periodo && r.Activo == true);
                
                data.Punto_Equilibrio = parametros.PuntoEquilibrio;
                data.Numero_Inscritos = parametros.NumeroInscritos;
                data.Usuario = parametros.Usuario;
                data.Fecha_Mod = Date;

                if (entidades.conexion.SaveChanges() > 0)
                {
                    Respuesta.Mensaje = "Programa actualizado con exito";
                    Respuesta.Status = true;
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Programa no actualizado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getProspecto")]
        [HttpPost]
        public RespuestaProspecto getProspecto(ParametrosProspecto parametros)
        {
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (data != null)
                {
                    data.ESTADO_CIVIL = data.ESTADO_CIVIL == null ? "" : data.ESTADO_CIVIL.TrimEnd(' ');
                    data.TIPO_SOLICITUD = data .TIPO_SOLICITUD == null ? "" : data.TIPO_SOLICITUD.TrimEnd(' ');
                    data.ESTADO_SOLICITUD = data.ESTADO_SOLICITUD == null ? "" : data.ESTADO_SOLICITUD.TrimEnd(' ');
                    Respuesta.Status = true;
                    Respuesta.Mensaje = "Prospecto encontrado";
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAllProspectos")]
        [HttpPost]
        public RespuestaProspectos getAllProspectos(ParametrosBuscarBeca parametros)
        {
            RespuestaProspectos Respuesta = new RespuestaProspectos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if(coordinador != null)
                {
                    long IdCoordinador = coordinador.Id;
                    try
                    {
                        List<string> tiposbeca = entidades.conexion.TP_CoordinadorBeca.Where(r => r.Id_Coordinador == IdCoordinador && r.Activo == true)
                            .Select(r => r.Tipo_Beca).Distinct().ToList();
                        if(tiposbeca.Count() > 0)
                        {
                            List<CB_Pospectos> allProspectos = new List<CB_Pospectos>();
                            foreach (string beca in tiposbeca)
                            {
                                List<CB_Pospectos> prospectos = entidades.conexion.CB_Pospectos.Where(r => r.TIPO_BECA == beca && r.Activo == true).ToList();
                                allProspectos.AddRange(prospectos);
                            }
                            if (allProspectos.Count() == 0)
                            {
                                Respuesta.Status = false;
                                Respuesta.Mensaje = "Prospectos no encontrados";
                                Respuesta.Lista = null;
                            }
                            else if (allProspectos != null)
                            {
                                foreach (CB_Pospectos prospecto in allProspectos)
                                {
                                    prospecto.ESTADO_CIVIL = prospecto.ESTADO_CIVIL == null ? "" : prospecto.ESTADO_CIVIL.TrimEnd(' ');
                                    prospecto.TIPO_SOLICITUD = prospecto.TIPO_SOLICITUD == null ? "" : prospecto.TIPO_SOLICITUD.TrimEnd(' ');
                                    prospecto.ESTADO_SOLICITUD = prospecto.ESTADO_SOLICITUD == null ? "" : prospecto.ESTADO_SOLICITUD.TrimEnd(' ');
                                }

                                Respuesta.Status = true;
                                Respuesta.Mensaje = "Prospectos encontrados";
                                Respuesta.Lista = allProspectos;
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Prospectos no encotrados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Coordinador no encotrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getSolicitud")]
        [HttpPost]
        public RespuestaProspecto getSolicitud(ParametrosProspecto parametros)
        {
            CB_Pospectos data = new CB_Pospectos();
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (data != null)
                {
                    Respuesta.Status = true;
                    Respuesta.Mensaje = "Solicitud encontrada";
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Solicitud no encontrada";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setSolicitud")]
        [HttpPost]
        public RespuestaProspecto insertarSolicitud(ParametrosSolicitud parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos nuevo = new CB_Pospectos
                {
                    EXPEDIENTE = parametros.Expediente,
                    PERIODO = parametros.Periodo,
                    NOMBRES = parametros.Nombres,
                    APELLIDOS = parametros.Apellidos,
                    GENERO = parametros.Genero,
                    NACIONALIDAD = parametros.Nacionalidad,
                    FECHA_NAC = parametros.FechaNac,
                    EDAD = parametros.Edad,
                    DOMICILIO = parametros.Domicilio,
                    EMAIL = parametros.Email,
                    TELEFONO_FIJO = parametros.Tel_Fijo,
                    TELEFONO_MOVIL = parametros.Tel_Movil,
                    TELEFONO_OFICINA = parametros.Tel_Oficina,
                    TIPO_SOLICITUD = parametros.TipoSolicitud,
                    ESTADO_SOLICITUD = "Iniciada",
                    TIPO_BECA = parametros.TipoBeca,
                    PORCENTAJE_BECA_PROPUESTO = parametros.PorcentajeBecaPropuesto,
                    CVE_PROGRAMA = parametros.CvePrograma.ToUpper(),
                    DESC_PROGRAMA = parametros.DescPrograma,
                    Activo = true,
                    Usuario = parametros.Usuario,
                    Fecha_Crea = Date,
                    Fecha_Mod = Date,
                };

                entidades.conexion.CB_Pospectos.Add(nuevo);
                if (entidades.conexion.SaveChanges() > 0)
                {
                    try
                    {
                        CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                        if(prospecto != null)
                        {
                            long IdProspecto = prospecto.ID;   
                            TP_AportaDepende aporta = new TP_AportaDepende
                            {
                                ID_PROSPECTO = IdProspecto,
                                Activo = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_AportaDepende.Add(aporta);
                            TP_Autos auto = new TP_Autos
                            {
                                ID_PROSPECTO = IdProspecto,
                                Activo = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_Autos.Add(auto);
                            TP_BienesInversiones bien = new TP_BienesInversiones
                            {
                                ID_PROSPECTO = IdProspecto,
                                Activo = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_BienesInversiones.Add(bien);
                            TP_Conyugues conyugue = new TP_Conyugues
                            {
                                ID_PROSPECTO = IdProspecto,
                                Activo = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_Conyugues.Add(conyugue);
                            TP_Documentos documentos = new TP_Documentos
                            {
                                ID_PROSPECTO = IdProspecto,
                                CARTA_COMP_PAGO = "No Cargado",
                                CARTA_ADMIN_PROG = "No Cargado",
                                CARTA_SOL_BECA = "No Cargado",
                                CALIF_ULT_GRADO = "No Cargado",
                                CARTA_ASIGNA_BECA = "No Cargado",
                                CARTA_JEFE = "No Cargado",
                                PLAN_TRABAJO = "No Cargado",
                                PLAN_TRABAJO_VICE = "No Cargado",
                                Activo = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_Documentos.Add(documentos);
                            TP_Empleos empleos = new TP_Empleos
                            {
                                ID_PROSPECTO = IdProspecto,
                                Activo = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_Empleos.Add(empleos);
                            TP_Gastos gastos = new TP_Gastos
                            {
                                ID_PROSPECTO = IdProspecto,
                                Activo = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_Gastos.Add(gastos);
                            TP_Grados grados = new TP_Grados
                            {
                                ID_PROSPECTO = IdProspecto,
                                Activo_LIC = true,
                                Activo_MAS = true,
                                Activo_DOC = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_Grados.Add(grados);
                            TP_Hijos hijos = new TP_Hijos
                            {
                                ID_PROSPECTO = IdProspecto,
                                Activo = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_Hijos.Add(hijos);
                            TP_Viviendas viviendas = new TP_Viviendas
                            {
                                ID_PROSPECTOS = IdProspecto,
                                Activo = true,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            entidades.conexion.TP_Viviendas.Add(viviendas);
                            if(entidades.conexion.SaveChanges() > 9)
                            {
                                Respuesta.Mensaje = "Solicitud insertada con exito";
                                Respuesta.Status = true;
                                Respuesta.Lista = nuevo;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Mensaje = "Error: Prospecto no insertado";
                        Respuesta.Status = false;
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Solicitud no insertada";
                Respuesta.Lista =  null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateSolicitud")]
        [HttpPost]
        public RespuestaProspecto updateSolicitud(ParametrosBeca parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                data.PORCENTAJE_BECA_PROPUESTO = parametros.PorcentajeBecaPropuesto;
                data.Fecha_Mod = Date;

                if (entidades.conexion.SaveChanges() > 0)
                {
                    Respuesta.Mensaje = "Solicitud actualizada con exito";
                    Respuesta.Status = true;
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Solicitud no actualizada";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setPorcentajeBeca")]
        [HttpPost]
        public  RespuestaProspecto insertarPorcentajeBeca(ParametrosBeca parametros)
        {
            CB_Pospectos data = new CB_Pospectos();
            DateTime Date = DateTime.Now;
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                data.PORCENTAJE_BECA_PROPUESTO = parametros.PorcentajeBecaPropuesto;
                data.Usuario = parametros.Usuario;
                data.Fecha_Mod = Date;
            }
            catch(Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Porcentaje de Beca no insertado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }
    }
}