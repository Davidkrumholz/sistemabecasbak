﻿using becasPosgrado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using static becasPosgrado.Models.BecasModel;

namespace becasPosgrado.Controllers
{
    [RoutePrefix("api/Becas")]
    public class BecasController : ApiController
    {
 
        public Helpers entidades = new Helpers();

        [Route("getDocumentos")]
        [HttpPost]
        public RespuestaDocumentos getDocumentos(ParametrosBuscar parametros)
        {
            RespuestaDocumentos Respuesta = new RespuestaDocumentos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Documentos data = entidades.conexion.TP_Documentos.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);

                        if (data != null)
                        {
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Documentos encontrados";
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Documentos no encontrados; Error: " + e;
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Prospecto no encontrado; Error: " + e;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateDocumentos")]
        [HttpPost]
        public RespuestaDocumentos updateDocumentos(ParametrosDocumentos parametros)
        {
            DateTime Date = DateTime.Now;
            CB_Pospectos prospecto = new CB_Pospectos();
            RespuestaDocumentos Respuesta = new RespuestaDocumentos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if(prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Documentos data = entidades.conexion.TP_Documentos.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);

                        data.CARTA_COMP_PAGO = parametros.CartaCompPago;
                        data.CARTA_ADMIN_PROG = parametros.CartaAdminProg;
                        data.CARTA_SOL_BECA = parametros.CartaSolBeca;
                        data.CALIF_ULT_GRADO = parametros.CalifUltimoGrado;
                        data.CARTA_ASIGNA_BECA = parametros.CartaAsignaBeca;
                        data.CARTA_JEFE = parametros.CartaJefe;
                        data.PLAN_TRABAJO = parametros.PlanTrabajo;
                        data.PLAN_TRABAJO_VICE = parametros.PlanTrabajoVice;
                        data.COM_CARTA_COM_PAGO = parametros.ComentarioCartaCompPago;
                        data.COM_CARTA_ADMIN_PROG = parametros.ComentarioCartaAdminProg;
                        data.COM_CARTA_SOL_BECA = parametros.ComentarioCartaSolBeca;
                        data.COM_CALIF_ULT_GRADO = parametros.ComentarioCalifUltimoGrado;
                        data.COM_CARTA_ASIGNA_BECA = parametros.ComentarioCartaAsignaBeca;
                        data.COM_CARTA_JEFE = parametros.ComentarioCartaJefe;
                        data.COM_PLAN_TRABAJO = parametros.ComentarioPlanTrabajo;
                        data.Usuario = parametros.Usuario;
                        data.Fecha_Mod = Date;

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Documentos actualizados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Documentos no actualizados; Error: " + e;
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Prospecto no encontrado; Error: " + e;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getProspecto")]
        [HttpPost]
        public RespuestaProspecto getProspecto(ParametrosBuscar parametros)
        {
            CB_Pospectos data = new CB_Pospectos();
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (data != null)
                {
                    data.ESTADO_CIVIL = data.ESTADO_CIVIL == null ? "" : data.ESTADO_CIVIL.TrimEnd(' ');
                    data.TIPO_SOLICITUD = data.TIPO_SOLICITUD == null ? "" : data.TIPO_SOLICITUD.TrimEnd(' ');
                    data.ESTADO_SOLICITUD = data.ESTADO_SOLICITUD == null ? "" : data.ESTADO_SOLICITUD.TrimEnd(' ');
                    Respuesta.Status = true;
                    Respuesta.Mensaje = "Prospecto encontrado";
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Prospecto no encontrado; Error: " + e;
                Respuesta.Lista = data = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAllProspectos")]
        [HttpPost]
        public RespuestaProspectos getAllProspectos()
        {
            RespuestaProspectos Respuesta = new RespuestaProspectos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                List<CB_Pospectos>  data = entidades.conexion.CB_Pospectos.Where(r => r.Activo == true).ToList();
                if (data.Count() == 0)
                {
                    Respuesta.Status = false;
                    Respuesta.Mensaje = "Prospectos no encontrados";
                    Respuesta.Lista = null;
                }
                else if (data != null)
                {
                    foreach (CB_Pospectos prospecto in data)
                    {
                        prospecto.ESTADO_CIVIL = prospecto.ESTADO_CIVIL == null ? "" : prospecto.ESTADO_CIVIL.TrimEnd(' ');
                        prospecto.TIPO_SOLICITUD = prospecto.TIPO_SOLICITUD == null ? "" : prospecto.TIPO_SOLICITUD.TrimEnd(' ');
                        prospecto.ESTADO_SOLICITUD = prospecto.ESTADO_SOLICITUD == null ? "" :  prospecto.ESTADO_SOLICITUD.TrimEnd(' ');
                    }
                    Respuesta.Status = true;
                    Respuesta.Mensaje = "Prospectos encontrados";
                    Respuesta.Lista = data;
                }
                
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospectos no encotrados";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setPorcentajeBeca")]
        [HttpPost]
        public RespuestaProspecto insertarPorcentajeBeca(ParametrosBeca parametros)
        {
            CB_Pospectos data = new CB_Pospectos();
            DateTime Date = DateTime.Now;
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                data.PORCENTAJE_BECA_ASIGNADO = parametros.PorcentajeBecaAsignado;
                data.ESTADO_SOLICITUD = parametros.EstadoSolicitud;
                data.Usuario = parametros.Usuario;
                data.Fecha_Mod = Date;

                if (entidades.conexion.SaveChanges() > 0)
                {
                    Respuesta.Mensaje = "Porcentaje de Beca asignado correctamente";
                    Respuesta.Status = true;
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Porcentaje de Beca no asignado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setEstadoSolicitud")]
        [HttpPost]
        public RespuestaProspecto insertarEstadoSolicitud(ParametrosSolicitud parametros)
        {
            CB_Pospectos data = new CB_Pospectos();
            DateTime Date = DateTime.Now;
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                data.ESTADO_SOLICITUD = parametros.EstadoSolicitud;
                data.Usuario = parametros.Usuario;
                data.Fecha_Mod = Date;

                if (entidades.conexion.SaveChanges() > 0)
                {
                    Respuesta.Mensaje = "Estado de la solicitud actualizado correctamente";
                    Respuesta.Status = true;
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Estado de la solicitud actualizado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAllCoordinadores")]
        [HttpPost]
        public RespuestaCoordinadores getCoordinadores()
        {
            RespuestaCoordinadores Respuesta = new RespuestaCoordinadores();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                List<TP_Usuarios> coordinadores = entidades.conexion.TP_Usuarios.Where(r => r.Activo == true).ToList();
                if (coordinadores.Count() == 0)
                {
                    Respuesta.Status = false;
                    Respuesta.Mensaje = "Coordinadores no encontrados";
                    Respuesta.Lista = null;
                }
                else if (coordinadores != null)
                {
                    foreach(TP_Usuarios coordinador in coordinadores)
                    {
                        coordinador.Tipo = coordinador.Tipo == null ? "" : coordinador.Tipo.TrimEnd(' ');
                    }
                    Respuesta.Mensaje = "Coordinadores encontrados";
                    Respuesta.Status = true;
                    Respuesta.Lista = coordinadores;
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinadores no encontrados";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getCoordinador")]
        [HttpPost]
        public RespuestaCoordinador getCoordinadores(ParametrosBuscar parametros)
        {
            RespuestaCoordinador Respuesta = new RespuestaCoordinador();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if (coordinador != null)
                {
                    coordinador.Tipo = coordinador.Tipo == null ? "" : coordinador.Tipo.TrimEnd(' ');
                    Respuesta.Mensaje = "Coordinador encontrado";
                    Respuesta.Status = true;
                    Respuesta.Lista = coordinador;
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinador no encontrado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setCoordinador")]
        [HttpPost]
        public RespuestaCoordinador insertarCoordinador(ParametrosCoordinador parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinador Respuesta = new RespuestaCoordinador();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios nuevo = new TP_Usuarios
                {
                    Expediente = parametros.Expediente,
                    Nombre = parametros.Nombre,
                    Tipo = parametros.Tipo,
                    Usuario = parametros.Usuario,
                    Activo = true,
                    Fecha_Crea = Date,
                    Fecha_Mod = Date
                };

                entidades.conexion.TP_Usuarios.Add(nuevo);

                if(entidades.conexion.SaveChanges() > 0)
                {
                    Respuesta.Mensaje = "Coordinador insertado correctamente";
                    Respuesta.Status = true;
                    Respuesta.Lista = nuevo;
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinador no insertado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateCoordinador")]
        [HttpPost]
        public RespuestaCoordinador updateCoordinador(ParametrosUpdateCoordinador parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinador Respuesta = new RespuestaCoordinador();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if (coordinador != null)
                {
                    coordinador.Tipo = parametros.Tipo;
                    coordinador.Nombre = parametros.Nombre;
                    coordinador.Usuario = parametros.Usuario;
                    coordinador.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Coordinador actualizado correctamente";
                        Respuesta.Status = true;
                        Respuesta.Lista = coordinador;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinador no actualizado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("deleteCoordinador")]
        [HttpPost]
        public RespuestaCoordinador deleteCoordinador(ParametrosBorrar parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinador Respuesta = new RespuestaCoordinador();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if(coordinador != null)
                {
                    coordinador.Activo = false;
                    coordinador.Usuario = parametros.Usuario;
                    coordinador.Fecha_Mod = Date;
                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Coordinador borrado correctamente";
                        Respuesta.Status = true;
                        Respuesta.Lista = coordinador;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinador no borrado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAllProgramas")]
        [HttpPost]
        public RespuestaProgramas getProgramas()
        {
            RespuestaProgramas Respuesta = new RespuestaProgramas();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                List<TP_Programas> programas = entidades.conexion.TP_Programas.Where(r => r.Activo == true)
                    .OrderBy(r => r.Id_Coordinador).ToList();
                if (programas.Count() == 0)
                {
                    Respuesta.Status = false;
                    Respuesta.Mensaje = "Coordinadores no encontrados";
                    Respuesta.Lista = null;
                }
                else if (programas != null)
                {
                    Respuesta.Mensaje = "Coordinadores encontrados";
                    Respuesta.Status = true;
                    Respuesta.Lista = programas;
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinadores no encontrados";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getCoordinadorPrograma")]
        [HttpPost]
        public RespuestaCoordinadorPrograma getCoordinadorPrograma(ParametrosBuscarPrograma parametros)
        {
            RespuestaCoordinadorPrograma Respuesta = new RespuestaCoordinadorPrograma();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_CoordinadorPrograma data = entidades.conexion.TP_CoordinadorPrograma.FirstOrDefault(r => r.Cve_Programa == parametros.CvePrograma.ToUpper() && r.Activo == true);
                if(data != null)
                {
                    Respuesta.Mensaje = "Coordinador y programa encontrado";
                    Respuesta.Status = true;
                    Respuesta.Lista = data;
                }
            }
            catch(Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Coordinador y programa no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAllCoordinadoresProgramas")]
        [HttpPost]
        public RespuestaCoordinadoresProgramas getCoordinadorPrograma()
        {
            RespuestaCoordinadoresProgramas Respuesta = new RespuestaCoordinadoresProgramas();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                List<TP_CoordinadorPrograma> data = entidades.conexion.TP_CoordinadorPrograma.Where(r => r.Activo == true)
                    .OrderBy(r => r.Id_Coordinador).ToList();
                if(data.Count() == 0)
                {
                    Respuesta.Mensaje = "Coordinadores y programas no encontrados";
                    Respuesta.Status = true;
                    Respuesta.Lista = null;
                }
                else if(data != null)
                {
                    Respuesta.Mensaje = "Coordinadores y programas encontrados";
                    Respuesta.Status = true;
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Coordinadores y programas no encontrados";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getCoordinadorProgramas")]
        [HttpPost]
        public RespuestaCoordinadoresProgramas getCoordinadorProgramas(ParametrosBuscar parametros)
        {
            RespuestaCoordinadoresProgramas Respuesta = new RespuestaCoordinadoresProgramas();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if(coordinador != null)
                {
                    long IdCoordinador = coordinador.Id;
                    try
                    {
                        List<TP_CoordinadorPrograma> data = entidades.conexion.TP_CoordinadorPrograma
                            .Where(r => r.Id_Coordinador == IdCoordinador && r.Activo == true).ToList();
                        if (data.Count() == 0)
                        {
                            Respuesta.Mensaje = "Programas asingados al Coordinador no encontrados";
                            Respuesta.Status = true;
                            Respuesta.Lista = null;
                        }
                        else if (data != null)
                        {
                            Respuesta.Mensaje = "Programas asingados al Coordinador encontrados";
                            Respuesta.Status = true;
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Programas asingados al Coordinador no encontrados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Coordinador no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setCoordinadorPrograma")]
        [HttpPost]
        public RespuestaCoordinadorPrograma insertarCoordinadorPrograma(ParametrosCoordinadorPrograma parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinadorPrograma Respuesta = new RespuestaCoordinadorPrograma();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_CoordinadorPrograma programa = entidades.conexion.TP_CoordinadorPrograma
                    .FirstOrDefault(r => r.Cve_Programa == parametros.CvePrograma.ToUpper() && r.Activo == true);
                if(programa != null)
                {
                    Respuesta.Mensaje = "Programa ya existente";
                    Respuesta.Status = false;
                    Respuesta.Lista = null;
                }
                else
                {
                    try
                    {
                        TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                        if (coordinador != null)
                        {
                            long IdCoordinador = coordinador.Id;
                            try
                            {
                                TP_CoordinadorPrograma nuevo = new TP_CoordinadorPrograma
                                {
                                    Id_Coordinador = IdCoordinador,
                                    Cve_Programa = parametros.CvePrograma.ToUpper(),
                                    Nombre_Programa = parametros.NombrePrograma,
                                    Usuario = parametros.Usuario,
                                    Activo = true,
                                    Fecha_Crea = Date,
                                    Fecha_Mod = Date
                                };

                                entidades.conexion.TP_CoordinadorPrograma.Add(nuevo);
                                if (entidades.conexion.SaveChanges() > 0)
                                {
                                    Respuesta.Status = true;
                                    Respuesta.Mensaje = "Coordinador y programa insertado correctamente";
                                    Respuesta.Lista = nuevo;
                                }
                            }
                            catch (Exception e)
                            {
                                Respuesta.Status = false;
                                Respuesta.Mensaje = "Error: Coordinador y programa no insertado";
                                Respuesta.Lista = null;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Coordinador y programa no insertado";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch(Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Coordinador y programa no insertado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateCoordinadorPrograma")]
        [HttpPost]
        public RespuestaCoordinadorPrograma updateCoordinadorPrograma(ParametrosUpdateCoordinadorPrograma parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinadorPrograma Respuesta = new RespuestaCoordinadorPrograma();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if(coordinador != null)
                {
                    long IdCoordinador = coordinador.Id;
                    try
                    {
                        TP_CoordinadorPrograma data = entidades.conexion.TP_CoordinadorPrograma.FirstOrDefault(r => r.Id == parametros.Id && r.Activo == true);
                        if (data != null)
                        {
                            data.Id_Coordinador = IdCoordinador;
                            data.Cve_Programa = parametros.CvePrograma.ToUpper();
                            data.Nombre_Programa = parametros.NombrePrograma;
                            data.Usuario = parametros.Usuario;
                            data.Fecha_Mod = Date;

                            if (entidades.conexion.SaveChanges() > 0)
                            {
                                Respuesta.Status = true;
                                Respuesta.Mensaje = "Coordinador y programa actualizado correctamente";
                                Respuesta.Lista = data;
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Coordinador y programa no actualizado";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch(Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Coordinador no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("deleteCoordinadorPrograma")]
        [HttpPost]
        public RespuestaCoordinadorPrograma borrarCoordinadorPrograma(ParametrosBorrarCoordinadorPrograma parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinadorPrograma Respuesta = new RespuestaCoordinadorPrograma();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_CoordinadorPrograma data = entidades.conexion.TP_CoordinadorPrograma.FirstOrDefault(r => r.Id == parametros.Id && r.Activo == true);
                if (data != null)
                {
                    data.Activo = false;
                    data.Usuario = parametros.Usuario;
                    data.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Status = true;
                        Respuesta.Mensaje = "Coordinador y programa borrado correctamente";
                        Respuesta.Lista = data;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Coordinador y programa no borrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("deletePuntoEquilibrio")]
        [HttpPost]
        public RespuestaPrograma deletePuntoEquilibrio(ParametrosPrograma parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaPrograma Respuesta = new RespuestaPrograma();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Programas data = entidades.conexion.TP_Programas
                    .FirstOrDefault(r => r.Cve_Programa == parametros.CvePrograma.ToUpper() && r.Periodo == parametros.Periodo && r.Activo == true);
                if(data != null)
                {
                    data.Activo = false;
                    data.Usuario = parametros.Usuario;
                    data.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Programa borrado con exito";
                        Respuesta.Status = true;
                        Respuesta.Lista = data;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Programa no borrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getMenus")]
        [HttpPost]
        public RespuestaMenus getMenus(ParametrosBuscar parametros)
        {
            string TipoUsuario = null;
            RespuestaMenus Respuesta = new RespuestaMenus();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios usuario = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if(usuario != null)
                {
                    TipoUsuario = usuario.Tipo;
                }
                else
                {
                    CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                    if (prospecto != null)
                    {
                        TipoUsuario = "Prospecto";
                    }
                    else
                    {
                        Respuesta.Mensaje = "Expediente no encontrado";
                        Respuesta.Status = false;
                        Respuesta.Lista = null;
                    }
                }
                if(TipoUsuario != null)
                {
                    try
                    {
                        List<VistaMenu> menu = entidades.conexion.VistaMenus.Where(r => r.TIPO_USUARIO == TipoUsuario && r.Activo == true).ToList();
                        if(menu.Count() == 0)
                        {
                            Respuesta.Mensaje = "Menus no encontrados";
                            Respuesta.Status = false;
                            Respuesta.Lista = null;
                        }
                        else if(menu != null)
                        {
                            foreach (VistaMenu menus in menu)
                            {
                                menus.TITULO = menus.TITULO == null ? "" : menus.TITULO.TrimEnd(' ');
                                menus.LINK = menus.LINK == null ? "" : menus.LINK.TrimEnd(' ');
                                menus.ICONO = menus.ICONO == null ? "" : menus.ICONO.TrimEnd(' ');
                            }
                            Respuesta.Mensaje = "Menus encontrados";
                            Respuesta.Status = true;
                            Respuesta.Lista = menu;
                        }
                    }
                    catch(Exception e)
                    {
                        Respuesta.Mensaje = "Error: Menus no encontrados";
                        Respuesta.Status = false;
                        Respuesta.Lista = null;
                    }
                }
            }
            catch(Exception e)
            {
                Respuesta.Mensaje = "Error: Expediente no encontrado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            return Respuesta;
        }

        [Route("getCoordinadorBeca")]
        [HttpPost]
        public RespuestaVistaCoordinadorBeca getCoordinadorBeca(ParametrosBuscar parametros)
        {
            RespuestaVistaCoordinadorBeca Respuesta = new RespuestaVistaCoordinadorBeca();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                VistaCoordinadorBeca coordinadorBeca = entidades.conexion.VistaCoordinadorBecas.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if (coordinadorBeca != null)
                {
                    Respuesta.Mensaje = "Coordinador de Beca encontrado";
                    Respuesta.Status = true;
                    Respuesta.Lista = coordinadorBeca;
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinador de Beca no encontrado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAllCoordinadoresBeca")]
        [HttpPost]
        public RespuestaCoordinadoresBeca getAllCoordinadoresBeca()
        {
            RespuestaCoordinadoresBeca Respuesta = new RespuestaCoordinadoresBeca();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                List<VistaCoordinadorBeca> coordinadores = entidades.conexion.VistaCoordinadorBecas.Where(r => r.Activo == true).OrderBy(r => r.Tipo_Beca).ToList();
                if(coordinadores.Count() == 0)
                {
                    Respuesta.Mensaje = "Coordinadores de Beca no encontrados";
                    Respuesta.Status = false;
                    Respuesta.Lista = null;
                }
                else if(coordinadores != null)
                {
                    foreach (VistaCoordinadorBeca coordinadorBeca in coordinadores)
                    {
                        coordinadorBeca.Tipo_Beca = coordinadorBeca.Tipo_Beca == null ? "" : coordinadorBeca.Tipo_Beca.TrimEnd(' ');
                        coordinadorBeca.Expediente = coordinadorBeca.Expediente == null ? "" : coordinadorBeca.Expediente.TrimEnd(' ');
                        coordinadorBeca.Nombres = coordinadorBeca.Nombres == null ? "" : coordinadorBeca.Nombres.TrimEnd(' ');
                        coordinadorBeca.Correo = coordinadorBeca.Correo == null ? "" : coordinadorBeca.Correo.TrimEnd(' ');
                    }
                    Respuesta.Mensaje = "Coordinadores de Beca encontrados";
                    Respuesta.Status = true;
                    Respuesta.Lista = coordinadores;
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinador de Beca no encontrado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setCoordinadorBeca")]
        [HttpPost]
        public RespuestaCoordinadorBeca insertarCoordinadorBeca(ParametrosCoordinadorBeca parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinadorBeca Respuesta = new RespuestaCoordinadorBeca();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if (coordinador != null)
                {
                    long IdCoordinador = coordinador.Id;
                    try
                    {
                        TP_CoordinadorBeca nuevo = new TP_CoordinadorBeca{
                            Id_Coordinador = IdCoordinador,
                            Nombres = parametros.Nombres,
                            Tipo_Beca = parametros.TipoBeca,
                            Correo = parametros.Correo,
                            Usuario = parametros.Usuario,
                            Activo = true,
                            Fecha_Crea = Date,
                            Fecha_Mod = Date
                        };
                        entidades.conexion.TP_CoordinadorBeca.Add(nuevo);
                        if (entidades.conexion.SaveChanges()>0)
                        {
                            Respuesta.Mensaje = "Coordinador de Beca insertado correctamente";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevo;
                        }
                    }
                    catch(Exception e)
                    {
                        Respuesta.Mensaje = "Error: Coordinador de Beca no insertado";
                        Respuesta.Status = false;
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinador no encontrado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateCoordinadorBeca")]
        [HttpPost]
        public RespuestaCoordinadorBeca updateCoordinadorBeca(ParametrosCoordinadorBeca parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinadorBeca Respuesta = new RespuestaCoordinadorBeca();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if (coordinador != null)
                {
                    long IdCoordinador = coordinador.Id;
                    try
                    {
                        TP_CoordinadorBeca coordinadorBeca = entidades.conexion.TP_CoordinadorBeca
                            .FirstOrDefault(r => r.Id_Coordinador == IdCoordinador && r.Activo == true);
                        if(coordinadorBeca != null)
                        {
                            coordinadorBeca.Nombres = parametros.Nombres;
                            coordinadorBeca.Tipo_Beca = parametros.TipoBeca;
                            coordinadorBeca.Correo = parametros.Correo;
                            coordinadorBeca.Usuario = parametros.Usuario;
                            coordinadorBeca.Fecha_Mod = Date;

                            if (entidades.conexion.SaveChanges() > 0)
                            {
                                Respuesta.Mensaje = "Coordinador de Beca actualizado correctamente";
                                Respuesta.Status = true;
                                Respuesta.Lista = coordinadorBeca;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Mensaje = "Error: Coordinador de Beca no actualizado";
                        Respuesta.Status = false;
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinador no encontrado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("deleteCoordinadorBeca")]
        [HttpPost]
        public RespuestaCoordinadorBeca deleteCoordinadorBeca(ParametrosBorrar parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaCoordinadorBeca Respuesta = new RespuestaCoordinadorBeca();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Usuarios coordinador = entidades.conexion.TP_Usuarios.FirstOrDefault(r => r.Expediente == parametros.Expediente && r.Activo == true);
                if (coordinador != null)
                {
                    long IdCoordinador = coordinador.Id;
                    try
                    {
                        TP_CoordinadorBeca coordinadorBeca = entidades.conexion.TP_CoordinadorBeca
                            .FirstOrDefault(r => r.Id_Coordinador == IdCoordinador && r.Activo == true);
                        if (coordinadorBeca != null)
                        {
                            coordinadorBeca.Activo = false;
                            coordinadorBeca.Fecha_Mod = Date;

                            if (entidades.conexion.SaveChanges() > 0)
                            {
                                Respuesta.Mensaje = "Coordinador de Beca borrado correctamente";
                                Respuesta.Status = true;
                                Respuesta.Lista = coordinadorBeca;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Mensaje = "Error: Coordinador de Beca no borrado";
                        Respuesta.Status = false;
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Coordinador no encontrado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAllComentariosCoordinador")]
        [HttpPost]
        public RespuestaComentarios getAllComentariosCoordinador(ParametrosBuscar parametros)
        {
            RespuestaComentarios Respuesta = new RespuestaComentarios();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                VistaComentario comentarios = entidades.conexion.VistaComentarios.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente);
                if(comentarios != null)
                {
                    Respuesta.Mensaje = "Comentarios encontrados";
                    Respuesta.Status = true;
                    Respuesta.Lista = comentarios;
                }
            }
            catch(Exception e)
            {
                Respuesta.Mensaje = "Error: Comentarios no encontrados";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getComentarioCoordinador")]
        [HttpPost]
        public RespuestaComentario getComentarioCoordinador(ParametrosGetComentario parametros)
        {
            RespuestaComentario Respuesta = new RespuestaComentario();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            List<string> comentario = null;
            try
            {
                switch (parametros.TipoCoordinador)
                {
                    case "Becas":
                        comentario = entidades.conexion.VistaComentarios.Where(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true)
                            .Select(r => r.COMENTARIOS_BECAS).ToList();
                        break;
                    case "Coordinador":
                        comentario = entidades.conexion.VistaComentarios.Where(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true)
                            .Select(r => r.COMENTARIOS_COORD).ToList();
                        break;
                    case "Comite":
                        comentario = entidades.conexion.VistaComentarios.Where(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true)
                            .Select(r => r.COMENTARIOS_COMITE).ToList();
                        break;
                }
                if (comentario != null)
                {
                    Respuesta.Mensaje = "Comentario encontrado";
                    Respuesta.Status = true;
                    Respuesta.Lista = comentario;
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Comentario no encontrado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setComentarioCoordinador")]
        [HttpPost]
        public RespuestaProspecto insertarComentarioCoordinadoor(ParametrosComentarios parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    switch (parametros.TipoCoordinador)
                    {
                        case "Becas":
                            prospecto.COMENTARIOS_BECAS = parametros.Comentario;
                            break;
                        case "Coordinador":
                            prospecto.COMENTARIOS_COORD = parametros.Comentario;
                            break;
                        case "Comite":
                            prospecto.COMENTARIOS_COMITE = parametros.Comentario;
                            break;
                    }
                    prospecto.Usuario = parametros.Usuario;
                    prospecto.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Comentario insertado correctamente";
                        Respuesta.Status = true;
                        Respuesta.Lista = prospecto;
                    }
                }
            }
            catch(Exception e)
            {
                Respuesta.Mensaje = "Error: Comentario no insertado";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getReporteProspectos")]
        [HttpPost]
        public RespuestaReporteProspectos getReporteProspectos()
        {
            RespuestaReporteProspectos Respuesta = new RespuestaReporteProspectos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                List<VistaReporteProspecto> prospectos =  entidades.conexion.VistaReporteProspectoes.Where(r => r.Activo ==true).ToList();
                if(prospectos.Count() == 0) {
                    Respuesta.Mensaje = "Prospectos no encontrados";
                    Respuesta.Status = false;
                    Respuesta.Lista = null;
                }
                else if (prospectos != null)
                {
                    Respuesta.Mensaje = "Prospectos encontrados";
                    Respuesta.Status = true;
                    Respuesta.Lista = prospectos;
                }
            }
            catch (Exception e)
            {
                Respuesta.Mensaje = "Error: Prospectos no encontrados";
                Respuesta.Status = false;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }
    }
}