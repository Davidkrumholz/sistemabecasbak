﻿using becasPosgrado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using static becasPosgrado.Models.ProspectoModel;

namespace becasPosgrado.Controllers
{
    [RoutePrefix("api/Prospecto")]
    public class ProspectoController : ApiController
    {
        public Helpers entidades = new Helpers();

        [Route("getVivienda")]
        [HttpPost]
        public RespuestaViviendas getVivienda(ParametrosExpediente parametros)
        {
            RespuestaViviendas Respuesta = new RespuestaViviendas();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Viviendas data = entidades.conexion.TP_Viviendas.FirstOrDefault(r => r.ID_PROSPECTOS == IdProspecto && r.Activo == true);

                        if (data != null)
                        {
                            data.TIPO_VIVIENDA = data.TIPO_VIVIENDA == null ? "" : data.TIPO_VIVIENDA.TrimEnd(' ');
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Vivienda encontrada";
                            Respuesta.Lista = data;
                        }

                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Vivienda no encontrada";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setVivienda")]
        [HttpPost]
        public RespuestaViviendas insertarVivienda(ParametrosViviendas parametros)
        {

            DateTime Date = DateTime.Now;
            RespuestaViviendas Respuesta = new RespuestaViviendas();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Viviendas nuevo = new TP_Viviendas
                        {
                            ID_PROSPECTOS = IdProspecto,
                            TIPO_VIVIENDA = parametros.TipoVivienda,
                            TERRENO = parametros.Terreno,
                            SUPERFICIE_CONSTRUIDA = parametros.SuperficieConstruida,
                            CONDICIONES = parametros.Condiciones,
                            VALOR = parametros.Valor,
                            Activo = true,
                            Usuario = parametros.Usuario,
                            Fecha_Crea = Date,
                            Fecha_Mod = Date
                        };

                        entidades.conexion.TP_Viviendas.Add(nuevo);

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Vivienda insertada con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevo;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Vivienda no insertada";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateVivienda")]
        [HttpPost]
        public RespuestaViviendas updateVivienda(ParametrosViviendas parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaViviendas Respuesta = new RespuestaViviendas();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Viviendas data = entidades.conexion.TP_Viviendas.FirstOrDefault(r => r.ID_PROSPECTOS == IdProspecto && r.Activo == true);
                        data.TIPO_VIVIENDA = parametros.TipoVivienda;
                        data.TERRENO = parametros.Terreno;
                        data.SUPERFICIE_CONSTRUIDA = parametros.SuperficieConstruida;
                        data.CONDICIONES = parametros.Condiciones;
                        data.VALOR = parametros.Valor;
                        data.Usuario = parametros.Usuario;
                        data.Fecha_Mod = Date;

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Vivienda actualizada con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Vivienda no actualizada";
                        Respuesta.Lista = null;
                    }

                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getHijos")]
        [HttpPost]
        public RespuestaHijos getHijos(ParametrosExpediente parametros)
        {
            RespuestaHijos Respuesta = new RespuestaHijos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        List<TP_Hijos> data = entidades.conexion.TP_Hijos.Where(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true).ToList();
                        if (data.Count() == 0)
                        {
                            Respuesta.Status = false;
                            Respuesta.Mensaje = "Hijos no encontrados";
                            Respuesta.Lista = null;
                        }
                        else if (data != null)
                        {
                            foreach(TP_Hijos hijo in data)
                            {
                                hijo.OCUPACION = hijo.OCUPACION == null ? "" : hijo.OCUPACION.TrimEnd(' ');
                            }
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Hijos encontrados";
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Hijos no encontrados";
                        Respuesta.Lista = null;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encotrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setHijos")]
        [HttpPost]
        public RespuestaHijos insertarHijos(ParametrosHijos parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaHijos Respuesta = new RespuestaHijos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        List<TP_Hijos> nuevos = new List<TP_Hijos>();
                        foreach (ParametrosHijo hijo in parametros.Hijos)
                        {
                            TP_Hijos nuevo = new TP_Hijos
                            {
                                ID_PROSPECTO = IdProspecto,
                                NOMBRE = hijo.Nombre,
                                EDAD = hijo.Edad,
                                OCUPACION = hijo.Ocupacion,
                                COLEGIATURA = hijo.Colegiatura,
                                PORCENTAGE_BECA = hijo.PorcenajeBeca,
                                INGRESO = hijo.Ingresos,
                                Activo = true,
                                Usuario = parametros.Usuario,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            nuevos.Add(nuevo);
                        }

                        entidades.conexion.TP_Hijos.AddRange(nuevos);
                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Hijos insertados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevos;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Hijos no registrados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encotrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateHijo")]
        [HttpPost]
        public RespuestaHijo updateHijo(ParametrosUpdateHijo parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaHijo Respuesta = new RespuestaHijo();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Hijos hijo = entidades.conexion.TP_Hijos.FirstOrDefault(r => r.ID == parametros.Id && r.Activo == true);

                if (hijo != null)
                {
                    hijo.NOMBRE = parametros.Nombre;
                    hijo.EDAD = parametros.Edad;
                    hijo.OCUPACION = parametros.Ocupacion;
                    hijo.COLEGIATURA = parametros.Colegiatura;
                    hijo.PORCENTAGE_BECA = parametros.PorcenajeBeca;
                    hijo.INGRESO = parametros.Ingresos;
                    hijo.Usuario = parametros.Usuario;
                    hijo.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Hijo actualizado con exito";
                        Respuesta.Status = true;
                        Respuesta.Lista = hijo;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Hijo no actualizado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("deleteHijo")]
        [HttpPost]
        public RespuestaHijo borrarHijo(ParametrosBorrar parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaHijo Respuesta = new RespuestaHijo();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Hijos hijo = entidades.conexion.TP_Hijos.FirstOrDefault(r => r.ID == parametros.Id && r.Activo == true);

                if (hijo != null)
                {
                    hijo.Activo = false;
                    hijo.Usuario = parametros.Usuario;
                    hijo.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Hijo borrado con exito";
                        Respuesta.Status = true;
                        Respuesta.Lista = hijo;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Hijo no borrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getGrados")]
        [HttpPost]
        public RespuestaGrados getGrados(ParametrosExpediente parametros)
        {
            RespuestaGrados Respuesta = new RespuestaGrados();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Grados data = entidades.conexion.TP_Grados.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo_LIC == true);
                        if (data != null)
                        {
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Grados encontrados";
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Grados no encontrados";
                        Respuesta.Lista = null;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setGrados")]
        [HttpPost]
        public RespuestaGrados insertarGrados(ParametrosGrados parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaGrados Respuesta = new RespuestaGrados();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Grados nuevo = new TP_Grados
                        {
                            ID_PROSPECTO = IdProspecto,
                            ULTIMO_GRADO = parametros.UltimoGrado,
                            EGRESADO_LIC = parametros.EgresadoLic,
                            PROMEDIO_LIC = parametros.PromedioLic,
                            TITULADO_LIC = parametros.TituladoLic,
                            EGRESADO_MAS = parametros.EgresadoMas,
                            PROMEDIO_MAS = parametros.PromedioMas,
                            TITULADO_MAS = parametros.TituladoMas,
                            EGRESADO_DOC = parametros.EgresadoDoc,
                            PROMEDIO_DOC = parametros.PromedioDoc,
                            TITULADO_DOC = parametros.TituladoDoc,
                            Activo_LIC = true,
                            Activo_MAS = true,
                            Activo_DOC = true,
                            Usuario = parametros.Usuario,
                            Fecha_Crea = Date,
                            Fecha_Mod = Date
                        };
                        entidades.conexion.TP_Grados.Add(nuevo);

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Grados insertados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevo;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Grados no insertados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateGrados")]
        [HttpPost]
        public RespuestaGrados updateGrados(ParametrosGrados parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaGrados Respuesta = new RespuestaGrados();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Grados data = entidades.conexion.TP_Grados.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo_MAS == true);

                        data.ULTIMO_GRADO = parametros.UltimoGrado;
                        data.EGRESADO_LIC = parametros.EgresadoLic;
                        data.PROMEDIO_LIC = parametros.PromedioLic;
                        data.TITULADO_LIC = parametros.TituladoLic;
                        data.EGRESADO_MAS = parametros.EgresadoMas;
                        data.PROMEDIO_MAS = parametros.PromedioMas;
                        data.TITULADO_MAS = parametros.TituladoMas;
                        data.EGRESADO_DOC = parametros.EgresadoDoc;
                        data.PROMEDIO_DOC = parametros.PromedioDoc;
                        data.TITULADO_DOC = parametros.TituladoDoc;
                        data.Usuario = parametros.Usuario;
                        data.Fecha_Mod = Date;

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Grados actualizados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Grados no actualizados";
                        Respuesta.Lista = null;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encotrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getGastos")]
        [HttpPost]
        public RespuestaGastos getGastos(ParametrosExpediente parametros)
        {
            RespuestaGastos Respuesta = new RespuestaGastos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    TP_Gastos data = new TP_Gastos();
                    try
                    {
                        data = entidades.conexion.TP_Gastos.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);
                        if (data != null)
                        {
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Gastos encontrados";
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Gastos no encontrados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encotrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setGastos")]
        [HttpPost]
        public RespuestaGastos insertarGastos(ParametrosGastos parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaGastos Respuesta = new RespuestaGastos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Gastos nuevo = new TP_Gastos
                        {
                            ID_PROSPECTO = IdProspecto,
                            RENTA_HIPOTECA = parametros.RentaHipoteca,
                            SERVICIOS = parametros.Servicios,
                            EDUCACION = parametros.Educacion,
                            MEDICOS = parametros.Medicos,
                            ALIMENTOS = parametros.Alimentos,
                            VESTIDO = parametros.Vestido,
                            SEGURO = parametros.Seguro,
                            DIVERSION = parametros.Diversion,
                            AHORRO = parametros.Ahorro,
                            CREDITOS_IMPUESTOS = parametros.CreditosImpuestos,
                            OTROS = parametros.Otros,
                            Activo = true,
                            Usuario = parametros.Usuario,
                            Fecha_Crea = Date,
                            Fecha_Mod = Date
                        };
                        entidades.conexion.TP_Gastos.Add(nuevo);

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Gastos insertados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevo;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Gastos no insertados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateGastos")]
        [HttpPost]
        public RespuestaGastos updateGastos(ParametrosGastos parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaGastos Respuesta = new RespuestaGastos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Gastos data = entidades.conexion.TP_Gastos.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);

                        data.RENTA_HIPOTECA = parametros.RentaHipoteca;
                        data.SERVICIOS = parametros.Servicios;
                        data.EDUCACION = parametros.Educacion;
                        data.MEDICOS = parametros.Medicos;
                        data.ALIMENTOS = parametros.Alimentos;
                        data.VESTIDO = parametros.Vestido;
                        data.SEGURO = parametros.Seguro;
                        data.DIVERSION = parametros.Diversion;
                        data.AHORRO = parametros.Ahorro;
                        data.CREDITOS_IMPUESTOS = parametros.CreditosImpuestos;
                        data.OTROS = parametros.Otros;
                        data.Usuario = parametros.Usuario;
                        data.Fecha_Mod = Date;

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Gastos actualizados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Gastos no actualizados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getEmpleos")]
        [HttpPost]
        public RespuestaEmpleos getEmpleos(ParametrosExpediente parametros)
        {
            RespuestaEmpleos Respuesta = new RespuestaEmpleos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    TP_Empleos data = new TP_Empleos();
                    try
                    {
                        data = entidades.conexion.TP_Empleos.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);
                        if (data != null)
                        {
                            data.NATURALEZA_EMPRESA = data.NATURALEZA_EMPRESA == null ? "" : data.NATURALEZA_EMPRESA.TrimEnd(' ');
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Empleo encontrado";
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Empleo no encontrado";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setEmpleos")]
        [HttpPost]
        public RespuestaEmpleos insertarEmpleos(ParametrosEmpleos parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaEmpleos Respuesta = new RespuestaEmpleos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Empleos nuevo = new TP_Empleos
                        {
                            ID_PROSPECTO = IdProspecto,
                            INGRESO_NETO = parametros.IngresoNeto,
                            NOMBRE_EMPRESA = parametros.NombreEmpresa,
                            NATURALEZA_EMPRESA = parametros.NaturalezaEmpresa,
                            PUESTO = parametros.Puesto,
                            ANIOS = parametros.Anios,
                            Activo = true,
                            Usuario = parametros.Usuario,
                            Fecha_Crea = Date,
                            Fecha_Mod = Date
                        };
                        entidades.conexion.TP_Empleos.Add(nuevo);

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Empleo insertado con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevo;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Empleo no insertado";
                        Respuesta.Lista = null;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateEmpleos")]
        [HttpPost]
        public RespuestaEmpleos updateEmpleos(ParametrosEmpleos parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaEmpleos Respuesta = new RespuestaEmpleos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Empleos data = entidades.conexion.TP_Empleos.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);

                        data.INGRESO_NETO = parametros.IngresoNeto;
                        data.NOMBRE_EMPRESA = parametros.NombreEmpresa;
                        data.NATURALEZA_EMPRESA = parametros.NaturalezaEmpresa;
                        data.PUESTO = parametros.Puesto;
                        data.ANIOS = parametros.Anios;
                        data.Usuario = parametros.Usuario;
                        data.Fecha_Mod = Date;

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Empleo actualizado con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Empleo no actualizado";
                        Respuesta.Lista = null;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getDocumentos")]
        [HttpPost]
        public RespuestaDocumentos getDocumentos(ParametrosExpediente parametros)
        {
            RespuestaDocumentos Respuesta = new RespuestaDocumentos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    TP_Documentos data = new TP_Documentos();
                    try
                    {
                        data = entidades.conexion.TP_Documentos.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);

                        if (data != null)
                        {
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Documentos encontrados";
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Documentos no encontrados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setDocumentos")]
        [HttpPost]
        public RespuestaDocumentos insertarDocumentos(ParametrosDocumentos parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaDocumentos Respuesta = new RespuestaDocumentos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Documentos documentos = new TP_Documentos
                        {
                            ID_PROSPECTO = IdProspecto,
                            CARTA_COMP_PAGO = parametros.CartaCompPago,
                            CARTA_ADMIN_PROG = parametros.CartaAdminProg,
                            CARTA_SOL_BECA = parametros.CartaSolBeca,
                            CALIF_ULT_GRADO = parametros.CalifUltimoGrado,
                            CARTA_ASIGNA_BECA = parametros.CartaAsignaBeca,
                            CARTA_JEFE = parametros.CartaJefe,
                            PLAN_TRABAJO = parametros.PlanTrabajo,
                            PLAN_TRABAJO_VICE = parametros.PlanTrabajoVice,
                            Activo = true,
                            Usuario = parametros.Usuario,
                            Fecha_Crea = Date,
                            Fecha_Mod = Date,
                        };
                        entidades.conexion.TP_Documentos.Add(documentos);

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Documentos insertados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = documentos;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Documentos no insertados";
                        Respuesta.Lista = null;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateDocumentos")]
        [HttpPost]
        public RespuestaDocumentos updateDocumentos(ParametrosDocumentos parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaDocumentos Respuesta = new RespuestaDocumentos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        TP_Documentos data = entidades.conexion.TP_Documentos.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);

                        data.CARTA_COMP_PAGO = parametros.CartaCompPago;
                        data.CARTA_ADMIN_PROG = parametros.CartaAdminProg;
                        data.CARTA_SOL_BECA = parametros.CartaSolBeca;
                        data.CALIF_ULT_GRADO = parametros.CalifUltimoGrado;
                        data.CARTA_ASIGNA_BECA = parametros.CartaAsignaBeca;
                        data.CARTA_JEFE = parametros.CartaJefe;
                        data.PLAN_TRABAJO = parametros.PlanTrabajo;
                        data.PLAN_TRABAJO_VICE = parametros.PlanTrabajoVice;
                        data.Usuario = parametros.Usuario;
                        data.Fecha_Mod = Date;

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Documentos actualizados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Documentos no actualuizados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encotrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getConyugues")]
        [HttpPost]
        public RespuestaConyugues getConyugues(ParametrosExpediente parametros)
        {
            RespuestaConyugues Respuesta = new RespuestaConyugues();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    TP_Conyugues data = new TP_Conyugues();
                    try
                    {
                        data = entidades.conexion.TP_Conyugues.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);
                        if (data != null)
                        {
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Conyugue encontrado";
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Conyugue no encontrado";
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setConyugues")]
        [HttpPost]
        public RespuestaConyugues insertarConyugues(ParametrosConyugues parametros)
        {
            DateTime Date = DateTime.Now;
            CB_Pospectos prospceto = new CB_Pospectos();
            RespuestaConyugues Respuesta = new RespuestaConyugues();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                prospceto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospceto != null)
                {
                    long IdProspecto = prospceto.ID;
                    try
                    {
                        TP_Conyugues nuevo = new TP_Conyugues
                        {
                            ID_PROSPECTO = IdProspecto,
                            NOMBRE = parametros.Nombre,
                            VIVE = parametros.Vive,
                            OCUPACION = parametros.Ocupacion,
                            TRABAJA = parametros.Trabaja,
                            PUESTO = parametros.Puesto,
                            EMPRESA = parametros.Empresa,
                            INGRESO_NETO = parametros.IngresoNeto,
                            Activo = true,
                            Usuario = parametros.Usuario,
                            Fecha_Crea = Date,
                            Fecha_Mod = Date
                        };
                        entidades.conexion.TP_Conyugues.Add(nuevo);

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Conyugue insertado con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevo;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Conyugue no insertado";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateConyugues")]
        [HttpPost]
        public RespuestaConyugues updateConyugues(ParametrosConyugues parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaConyugues Respuesta = new RespuestaConyugues();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try {
                        TP_Conyugues data = entidades.conexion.TP_Conyugues.FirstOrDefault(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true);

                        data.NOMBRE = parametros.Nombre;
                        data.VIVE = parametros.Vive;
                        data.OCUPACION = parametros.Ocupacion;
                        data.TRABAJA = parametros.Trabaja;
                        data.PUESTO = parametros.Puesto;
                        data.EMPRESA = parametros.Empresa;
                        data.INGRESO_NETO = parametros.IngresoNeto;
                        data.Usuario = parametros.Usuario;
                        data.Fecha_Mod = Date;

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Conyugue actualizado con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Conyugue no actualizado";
                        Respuesta.Lista = null;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getBienesInversiones")]
        [HttpPost]
        public RespuestaBienesInversiones getBienesInversiones(ParametrosExpediente parametros)
        {
            RespuestaBienesInversiones Respuesta = new RespuestaBienesInversiones();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        List<TP_BienesInversiones>  data = entidades.conexion.TP_BienesInversiones.Where(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true).ToList();

                        if (data.Count() == 0)
                        {
                            Respuesta.Status = false;
                            Respuesta.Mensaje = "Bienes o inversiones no encontrados";
                            Respuesta.Lista = null;
                        }
                        else if (data != null)
                        {
                           foreach(TP_BienesInversiones bien in data)
                            {
                                bien.TIPO = bien.TIPO == null ? "" : bien.TIPO.TrimEnd(' ');
                            }
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Bienes o inversiones encontrados";
                            Respuesta.Lista = data;
                        }

                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Bienes o inversiones no encontrados " + e;
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }


        [Route("setBienesInversiones")]
        [HttpPost]
        public RespuestaBienesInversiones insertarBienesInversiones(ParametrosBienesInversiones parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaBienesInversiones Respuesta = new RespuestaBienesInversiones();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        List<TP_BienesInversiones> nuevos = new List<TP_BienesInversiones>();
                        foreach (ParametrosBienInversion bienesinv in parametros.BienesInversiones)
                        {
                            TP_BienesInversiones nuevo = new TP_BienesInversiones
                            {
                                ID_PROSPECTO = IdProspecto,
                                TIPO = bienesinv.Tipo,
                                VALOR = bienesinv.Valor,
                                Activo = true,
                                Usuario = parametros.Usuario,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            nuevos.Add(nuevo);
                        }

                        entidades.conexion.TP_BienesInversiones.AddRange(nuevos);

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Bienes o inversiones insertados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevos;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Bienes o inversiones no insertados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encotrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateBienesInversiones")]
        [HttpPost]
        public RespuestaBienInversion updateBienesInversiones(ParametrosUpdateBienesInversiones parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaBienInversion Respuesta = new RespuestaBienInversion();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_BienesInversiones data = entidades.conexion.TP_BienesInversiones.FirstOrDefault(r => r.ID == parametros.Id && r.Activo == true);
                if(data != null)
                {
                    data.TIPO = parametros.Tipo;
                    data.VALOR = parametros.Valor;
                    data.Usuario = parametros.Usuario;
                    data.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Bien o inversion actualizado con exito";
                        Respuesta.Status = true;
                        Respuesta.Lista = data;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Bien o inversion no actualizado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("deleteBienesInversiones")]
        [HttpPost]
        public RespuestaBienInversion borrarBienesInversiones(ParametrosBorrar parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaBienInversion Respuesta = new RespuestaBienInversion();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_BienesInversiones data = entidades.conexion.TP_BienesInversiones.FirstOrDefault(r => r.ID == parametros.Id && r.Activo == true);
                if (data != null)
                {
                    data.Activo = false;
                    data.Usuario = parametros.Usuario;
                    data.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Bien o inversion borrado con exito";
                        Respuesta.Status = true;
                        Respuesta.Lista = data;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Bien o inversion no borrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAutos")]
        [HttpPost]
        public RespuestaAutos getAutos(ParametrosExpediente parametros)
        {
            RespuestaAutos Respuesta = new RespuestaAutos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    List<TP_Autos> data = new List<TP_Autos>();
                    try
                    {
                        data = entidades.conexion.TP_Autos.Where(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true).ToList();

                        if (data.Count() == 0)
                        {
                            Respuesta.Status = false;
                            Respuesta.Mensaje = "Autos no encontrados";
                            Respuesta.Lista = null;
                        }
                        else if (data != null)
                        {
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Autos encontrados";
                            Respuesta.Lista = data;
                        }

                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Autos no encontrados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setAutos")]
        [HttpPost]
        public RespuestaAutos insertarAutos(ParametrosAutos parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaAutos Respuesta = new RespuestaAutos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    List<TP_Autos> nuevos = new List<TP_Autos>();
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        foreach (ParametrosAuto auto in parametros.Autos)
                        {
                            TP_Autos nuevo = new TP_Autos
                            {
                                ID_PROSPECTO = IdProspecto,
                                MARCA_MODELO = auto.MarcaModelo,
                                TIPO_AUTO = auto.TipoAuto,
                                ANIO = auto.Anio,
                                COSTO = auto.Costo,
                                PAGADO = auto.Pagado,
                                FINANCIAMIENTO = auto.Financiamiento,
                                MESES = auto.Meses,
                                Activo = true,
                                Usuario = parametros.Usuario,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            nuevos.Add(nuevo);
                        }
                        entidades.conexion.TP_Autos.AddRange(nuevos);

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Autos insertados con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevos;
                        }

                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Autos no insertados";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no enontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateAuto")]
        [HttpPost]
        public RespuestaAutos updateAuto(ParametrosUpdateAuto parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaAutos Respuesta = new RespuestaAutos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Autos auto = entidades.conexion.TP_Autos.FirstOrDefault(r => r.ID == parametros.Id && r.Activo == true );
                if( auto != null)
                {
                    auto.MARCA_MODELO = parametros.MarcaModelo;
                    auto.TIPO_AUTO = parametros.TipoAuto;
                    auto.ANIO = parametros.Anio;
                    auto.COSTO = parametros.Costo;
                    auto.PAGADO = parametros.Pagado;
                    auto.FINANCIAMIENTO = parametros.Financiamiento;
                    auto.MESES = parametros.Meses;
                    auto.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Autos actualizados con exito";
                        Respuesta.Status = true;
                        Respuesta.Lista = new List<TP_Autos>();
                        Respuesta.Lista.Add(auto);
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Auto no actualizados; Error:" + e;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("deleteAuto")]
        [HttpPost]
        public RespuestaAutos borrarAuto(ParametrosBorrar parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaAutos Respuesta = new RespuestaAutos();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_Autos auto = entidades.conexion.TP_Autos.FirstOrDefault(r => r.ID == parametros.Id && r.Activo == true);
                if (auto != null)
                {
                    auto.Activo = false;
                    auto.Usuario = parametros.Usuario;
                    auto.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Auto borrado con exito";
                        Respuesta.Status = true;
                        Respuesta.Lista = new List<TP_Autos>();
                        Respuesta.Lista.Add(auto);
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Auto no borrado; Error:" + e;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getAportaDepende")]
        [HttpPost]
        public RespuestaAportanDependen getAportaDepende(ParametrosExpediente parametros)
        {
            RespuestaAportanDependen Respuesta = new RespuestaAportanDependen();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        List<TP_AportaDepende>  data = entidades.conexion.TP_AportaDepende.Where(r => r.ID_PROSPECTO == IdProspecto && r.Activo == true).ToList();

                        if (data.Count() == 0)
                        {
                            Respuesta.Status = false;
                            Respuesta.Mensaje = "Persona que aporta o depende no encontrada";
                            Respuesta.Lista = null;
                        }
                        else if (data != null)
                        {
                            foreach(TP_AportaDepende persona in data)
                            {
                                persona.TIPO = persona.TIPO == null ? "" : persona.TIPO.TrimEnd(' ');
                            }
                            Respuesta.Status = true;
                            Respuesta.Mensaje = "Personas que aportan o dependen encontrados";
                            Respuesta.Lista = data;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Personas que aportan o dependen no encontradas";
                        Respuesta.Lista = null;
                    }
                }

            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("setAportaDepende")]
        [HttpPost]
        public RespuestaAportanDependen insertarAportaDepende(ParametrosAportanDependen parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaAportanDependen Respuesta = new RespuestaAportanDependen();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos prospecto = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);
                if (prospecto != null)
                {
                    List<TP_AportaDepende> nuevos = new List<TP_AportaDepende>();
                    long IdProspecto = prospecto.ID;
                    try
                    {
                        foreach (ParametrosAportaDepende persona in parametros.Personas)
                        {
                            TP_AportaDepende nuevo = new TP_AportaDepende
                            {
                                ID_PROSPECTO = IdProspecto,
                                TIPO = persona.Tipo,
                                QUIEN = persona.Quien,
                                Activo = true,
                                Usuario = parametros.Usuario,
                                Fecha_Crea = Date,
                                Fecha_Mod = Date
                            };
                            nuevos.Add(nuevo);
                        }

                        entidades.conexion.TP_AportaDepende.AddRange(nuevos);

                        if (entidades.conexion.SaveChanges() > 0)
                        {
                            Respuesta.Mensaje = "Persona que aporta o depende insertada con exito";
                            Respuesta.Status = true;
                            Respuesta.Lista = nuevos;
                        }
                    }
                    catch (Exception e)
                    {
                        Respuesta.Status = false;
                        Respuesta.Mensaje = "Error: Persona que aporta o depende no insertada";
                        Respuesta.Lista = null;
                    }
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no encontrado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateAportaDepende")]
        [HttpPost]
        public RespuestaAportaDepende updateAportaDepende(ParametrosUpdateAportaDepende parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaAportaDepende Respuesta = new RespuestaAportaDepende();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_AportaDepende data = entidades.conexion.TP_AportaDepende.FirstOrDefault(r => r.ID == parametros.Id && r.Activo == true);
                if(data != null) {

                    data.TIPO = parametros.Tipo;
                    data.QUIEN = parametros.Quien;
                    data.Usuario = parametros.Usuario;
                    data.Fecha_Mod = Date;
                    
                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Personas que aportan o dependen actualizadas con exito";
                        Respuesta.Status = true;
                        Respuesta.Lista = data;
                    }
                        
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Persona que aporta o depende no actualizada";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("deleteAportaDepende")]
        [HttpPost]
        public RespuestaAportaDepende borrarAportaDepende(ParametrosBorrar parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaAportaDepende Respuesta = new RespuestaAportaDepende();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                TP_AportaDepende data = entidades.conexion.TP_AportaDepende.FirstOrDefault(r => r.ID == parametros.Id && r.Activo == true);
                if (data != null)
                {

                    data.Activo = false;
                    data.Usuario = parametros.Usuario;
                    data.Fecha_Mod = Date;

                    if (entidades.conexion.SaveChanges() > 0)
                    {
                        Respuesta.Mensaje = "Persona que aportan o dependen borrada con exito";
                        Respuesta.Status = true;
                        Respuesta.Lista = data;
                    }

                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Persona que aporta o depende no borrada";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("getProspecto")]
        [HttpPost]
        public RespuestaProspecto getProspecto(ParametrosExpediente parametros)
        {
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente & r.Activo == true);
                if (data != null)
                {
                    data.ESTADO_CIVIL = data.ESTADO_CIVIL == null ? "" : data.ESTADO_CIVIL.TrimEnd(' ');
                    data.TIPO_SOLICITUD = data.TIPO_SOLICITUD == null ? "" : data.TIPO_SOLICITUD.TrimEnd(' ');
                    data.ESTADO_SOLICITUD = data.ESTADO_SOLICITUD == null ? "" : data.ESTADO_SOLICITUD.TrimEnd(' ');
                    Respuesta.Status = true;
                    Respuesta.Mensaje = "Prospecto encontrado";
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Prospecto no encontrado; Error:" + e;
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateSolicitud")]
        [HttpPost]
        public RespuestaProspecto updateSolicitud(ParametrosSolicitud parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                data.PORCENTAJE_BECA_SOLICITADO = parametros.PorcentajeBecaSolicitado;
                data.TIPO_SOLICITUD =  parametros.TipoSolicitud;
                data.Usuario = parametros.Usuario;
                data.Fecha_Mod = Date;

                if (entidades.conexion.SaveChanges() > 0)
                {
                    Respuesta.Mensaje = "Prospecto actualizado con exito";
                    Respuesta.Status = true;
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Prospecto no actualizado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }


        [Route("updateTelefonos")]
        [HttpPost]
        public RespuestaProspecto updateTelefonos(ParametrosTelefonos parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                data.TELEFONO_FIJO = parametros.Tel_Fijo;
                data.TELEFONO_MOVIL = parametros.Tel_Movil;
                data.TELEFONO_OFICINA = parametros.Tel_Oficina;
                data.Usuario = parametros.Usuario;
                data.Fecha_Mod = Date;

                if (entidades.conexion.SaveChanges() > 0)
                {
                    Respuesta.Mensaje = "Telefonos actualizados con exito";
                    Respuesta.Status = true;
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Telefonos no actualizado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }

        [Route("updateComentario")]
        [HttpPost]
        public RespuestaProspecto updateComentario(ParametrosComentario parametros)
        {
            DateTime Date = DateTime.Now;
            RespuestaProspecto Respuesta = new RespuestaProspecto();
            Respuesta.Status = false;
            Respuesta.Mensaje = "";
            try
            {
                CB_Pospectos data = entidades.conexion.CB_Pospectos.FirstOrDefault(r => r.EXPEDIENTE == parametros.Expediente && r.Activo == true);

                data.COMENTARIOS_PROSPECTO = parametros.Comentario;
                data.Usuario = parametros.Usuario;
                data.Fecha_Mod = Date;

                if (entidades.conexion.SaveChanges() > 0)
                {
                    Respuesta.Mensaje = "Comentario actualizado con exito";
                    Respuesta.Status = true;
                    Respuesta.Lista = data;
                }
            }
            catch (Exception e)
            {
                Respuesta.Status = false;
                Respuesta.Mensaje = "Error: Comentario no actualizado";
                Respuesta.Lista = null;
            }
            finally
            {
                entidades.conexion.Dispose();
            }
            return Respuesta;
        }
    }
}