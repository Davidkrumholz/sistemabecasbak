﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace becasPosgrado
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class BecasPosgradoEntities : DbContext
    {
        public BecasPosgradoEntities()
            : base("name=BecasPosgradoEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Cat_Menus> Cat_Menus { get; set; }
        public virtual DbSet<CB_Pospectos> CB_Pospectos { get; set; }
        public virtual DbSet<TP_AportaDepende> TP_AportaDepende { get; set; }
        public virtual DbSet<TP_Autos> TP_Autos { get; set; }
        public virtual DbSet<TP_BienesInversiones> TP_BienesInversiones { get; set; }
        public virtual DbSet<TP_Conyugues> TP_Conyugues { get; set; }
        public virtual DbSet<TP_CoordinadorBeca> TP_CoordinadorBeca { get; set; }
        public virtual DbSet<TP_CoordinadorPrograma> TP_CoordinadorPrograma { get; set; }
        public virtual DbSet<TP_Documentos> TP_Documentos { get; set; }
        public virtual DbSet<TP_Empleos> TP_Empleos { get; set; }
        public virtual DbSet<TP_Gastos> TP_Gastos { get; set; }
        public virtual DbSet<TP_Grados> TP_Grados { get; set; }
        public virtual DbSet<TP_Hijos> TP_Hijos { get; set; }
        public virtual DbSet<TP_Programas> TP_Programas { get; set; }
        public virtual DbSet<TP_Usuarios> TP_Usuarios { get; set; }
        public virtual DbSet<TP_Viviendas> TP_Viviendas { get; set; }
        public virtual DbSet<Usuarios_Menu> Usuarios_Menu { get; set; }
        public virtual DbSet<Variable> Variables { get; set; }
        public virtual DbSet<VistaComentario> VistaComentarios { get; set; }
        public virtual DbSet<VistaCoordinadorBeca> VistaCoordinadorBecas { get; set; }
        public virtual DbSet<VistaMenu> VistaMenus { get; set; }
        public virtual DbSet<VistaReporteProspecto> VistaReporteProspectoes { get; set; }
    }
}
