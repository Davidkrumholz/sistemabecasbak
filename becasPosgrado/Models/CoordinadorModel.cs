﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace becasPosgrado.Models
{
    public class CoordinadorModel
    {
        public class RespuestaProspecto
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public CB_Pospectos Lista { get; set; }
        }

        public class RespuestaProspectos
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<CB_Pospectos> Lista { get; set; }
        }

        public class RespuestaCoordinador
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Programas Lista { get; set; }
        }

        public class RespuestaPrograma
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<TP_Programas> Lista { get; set; }
        }

        public class ParametrosCoordinador
        {
            public string Periodo { get; set; }
            public string CvePrograma { get; set; }
            public byte PuntoEquilibrio { get; set; }
            public int NumeroInscritos { get; set; }
            public string Usuario { get; set; }

        }

        public class ParametrosNuevo
        {
            public string Periodo { get; set; }
            public string CvePrograma { get; set; }
            public string NombrePrograma { get; set; }
            public byte PuntoEquilibrio { get; set; }
            public int NumeroInscritos { get; set; }
            public string Expediente { get; set; }
            public string Usuario { get; set; }

        }

        public class ParametrosProspecto
        {
            public string Expediente { get; set; }

        }

        public class ParametrosBuscarBeca
        {
            public string Expediente { get; set; }
        }

        public class ParametrosSolicitud
        {
            public string Periodo { get; set; }
            public string Expediente { get; set; }
            public string Nombres { get; set; }
            public string Apellidos { get; set; }
            public string Genero { get; set; }
            public byte Edad { get; set; }
            public DateTime FechaNac { get; set; }
            public string Nacionalidad { get; set; }
            public string Domicilio { get; set; }
            public string Email { get; set; }
            public string Tel_Fijo { get; set; }
            public string Tel_Movil { get; set; }
            public string Tel_Oficina { get; set; }
            public string TipoSolicitud { get; set; }
            public string TipoBeca { get; set; }
            public byte PorcentajeBecaPropuesto { get; set; }
            public string CvePrograma { get; set; }
            public string DescPrograma { get; set; }
            public string Usuario { get; set; }
        }
  
        public class ParametrosPrograma
        {
            public string Expediete { get; set; }
            public string Periodo { get; set; }
        }

        public class ParametrosBeca
        {
            public string Expediente { get; set; }
            public byte PorcentajeBecaPropuesto { get; set; }
            public string Usuario { get; set; }

        }
    }
}