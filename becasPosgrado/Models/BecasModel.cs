﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace becasPosgrado.Models
{
    public class BecasModel
    {
        public class RespuestaDocumentos
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Documentos Lista { get; set; }
        }

        public class RespuestaProspecto
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public CB_Pospectos Lista { get; set; }
        }

        public class RespuestaProspectos
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<CB_Pospectos> Lista { get; set; }
        }

        public class RespuestaProgramas
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<TP_Programas> Lista { get; set; }
        }

        public class RespuestaMenus
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<VistaMenu> Lista { get; set; }
        }
        public class RespuestaCoordinadores
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<TP_Usuarios> Lista { get; set; }
        }

        public class RespuestaCoordinador
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Usuarios Lista { get; set; }
        }

        public class RespuestaPrograma
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Programas Lista { get; set; }
        }

        public class RespuestaCoordinadorPrograma
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_CoordinadorPrograma Lista { get; set; }
        }

        public class RespuestaCoordinadoresProgramas
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<TP_CoordinadorPrograma> Lista { get; set; }
        }

        public class RespuestaCoordinadorBeca
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_CoordinadorBeca Lista { get; set; }
        }

        public class RespuestaVistaCoordinadorBeca
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public VistaCoordinadorBeca Lista { get; set; }
        }

        public class RespuestaCoordinadoresBeca
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<VistaCoordinadorBeca> Lista { get; set; }
        }

        public class RespuestaComentario
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<string> Lista { get; set; }
        }

        public class RespuestaComentarios
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public VistaComentario Lista { get; set; }
        }

        public class RespuestaReporteProspectos
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<VistaReporteProspecto> Lista { get; set; }
        }

        public class ParametrosCoordinadorBeca
        {
            public string Expediente { get; set; }
            public string Nombres { get; set; }
            public string TipoBeca { get; set; }
            public string Correo { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosComentarios
        {
            public string Expediente { get; set; }
            public string TipoCoordinador { get; set; }
            public string Comentario { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosGetComentario
        {
            public string Expediente { get; set; }
            public string TipoCoordinador { get; set; }
        }

        public class ParametrosComentario
        {
            public string Expediente { get; set; }
            public byte IdCoordinador { get; set; }
        }

        public class ParametrosDocumentos
        {
            public string Expediente { get; set; }
            public string CartaCompPago { get; set; }
            public string CartaAdminProg { get; set; }
            public string CartaSolBeca { get; set; }
            public string CalifUltimoGrado { get; set; }
            public string CartaAsignaBeca { get; set; }
            public string CartaJefe { get; set; }
            public string PlanTrabajo { get; set; }
            public string PlanTrabajoVice { get; set; }
            public string ComentarioCartaCompPago { get; set; }
            public string ComentarioCartaAdminProg { get; set; }
            public string ComentarioCartaSolBeca { get; set; }
            public string ComentarioCalifUltimoGrado { get; set; }
            public string ComentarioCartaAsignaBeca { get; set; }
            public string ComentarioCartaJefe { get; set; }
            public string ComentarioPlanTrabajo { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosProspecto
        {
            public string Periodo { get; set; }
            public string Expediente { get; set; }
            public string TipoBeca { get; set; }
            public byte PorcentajeBecaAsignado { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosSolicitud
        {
            public string Expediente { get; set; }
            public string EstadoSolicitud { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosBeca
        {
            public string Expediente { get; set; }
            public string EstadoSolicitud { get; set; }
            public byte PorcentajeBecaAsignado { get; set; }
            public string Usuario { get; set; }

        }

        public class ParametrosCoordinador
        {
            public string Expediente { get; set; }
            public string Nombre { get; set; }
            public string Tipo { get; set; }
            public string Usuario { get; set; }
            
        }

        public class ParametrosUpdateCoordinador
        {
            public string Expediente { get; set; }
            public string Nombre { get; set; }
            public string Tipo { get; set; }
            public string Usuario { get; set; }

        }

        public class ParametrosCoordinadorPrograma
        {
            public string Expediente { get; set; }
            public string CvePrograma { get; set; }
            public string NombrePrograma { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosUpdateCoordinadorPrograma
        {
            public long Id { get; set; }
            public string Expediente { get; set; }
            public string CvePrograma { get; set; }
            public string NombrePrograma { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosBorrarCoordinadorPrograma
        {
            public long Id { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosPrograma
        {
            public string CvePrograma { get; set; }
            public string Periodo { get; set; }
            public string Usuario { get; set; }

        }

        public class ParametrosBorrar
        {
            public string Expediente { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosBuscarPrograma
        {
            public string CvePrograma { get; set; }
        }

        public class ParametrosBuscar
        {
            public string Expediente { get; set; }
        }
    }
}