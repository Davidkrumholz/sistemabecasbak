﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace becasPosgrado.Models
{
    public class ProspectoModel
    {
        public class RespuestaProspecto
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public CB_Pospectos Lista { get; set; }
        }
        public class RespuestaAportaDepende
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_AportaDepende Lista { get; set; }
        }
        public class RespuestaAportanDependen
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<TP_AportaDepende> Lista { get; set; }
        }
        public class RespuestaAutos
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<TP_Autos> Lista { get; set; }
        }
        public class RespuestaBienesInversiones
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<TP_BienesInversiones> Lista { get; set; }
        }
        public class RespuestaBienInversion
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_BienesInversiones Lista { get; set; }
        }
        public class RespuestaConyugues
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Conyugues Lista { get; set; }
        }


        public class RespuestaDocumentos
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Documentos Lista { get; set; }
        }
        public class RespuestaEmpleos
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Empleos Lista { get; set; }
        }
        public class RespuestaGastos
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Gastos Lista { get; set; }
        }

        public class RespuestaGrados
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Grados Lista { get; set; }
        }
        public class RespuestaHijos
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public List<TP_Hijos> Lista { get; set; }
        }
        public class RespuestaHijo
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Hijos Lista { get; set; }
        }
        public class RespuestaViviendas
        {
            public string Mensaje { get; set; }
            public bool Status { get; set; }
            public TP_Viviendas Lista { get; set; }
        }
        public class ParametrosProspecto
        {
            public string Periodo { get; set; }
            public string Expediente { get; set; }
            public string Nombres { get; set; }
            public string Apellidos { get; set; }
            public string Genero { get; set; }
            public string EstadoCivil { get; set; }
            public byte Edad { get; set; }
            public DateTime Fecha_Nac { get; set; }
            public string Nacionalidad { get; set; }
            public string Domicilio { get; set; }
            public string Email { get; set; }
            public string TipoBeca { get; set; }
            public byte PorcentajeBecaSolicitado { get; set; }
            public string TipoSolicitud { get; set; }
            public string Cve_Programa { get; set; }
            public string Desc_Programa { get; set; }
            public string Comentarios { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosSolicitud
        {
            public string Expediente { get; set; }
            public byte PorcentajeBecaSolicitado { get; set; }
            public string TipoSolicitud { get; set; }
            public string Usuario { get; set; }


        }

        public class ParametrosTelefonos
        {
            public string Expediente { get; set; }
            public string Tel_Fijo { get; set; }
            public string Tel_Movil { get; set; }
            public string Tel_Oficina { get; set; }
            public string Usuario { get; set; }

        }

        public class ParametrosComentario
        {
            public string Expediente { get; set; }
            public string Comentario { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosUpdateAportaDepende
        {
            public long Id { get; set; }
            public string Expediente { get; set; }
            public string Tipo { get; set; }
            public string Quien { get; set; }
            public string Usuario { get; set; }

        }

        public class ParametrosAportanDependen
        {
            public string Expediente { get; set; }
            public string Usuario { get; set; }
            public List<ParametrosAportaDepende> Personas { get; set; }

        }
        public class ParametrosAportaDepende
        {
            public string Tipo { get; set; }
            public string Quien { get; set; }
        }
        public class ParametrosUpdateAuto
        {
            public long Id { get; set; }
            public string MarcaModelo { get; set; }
            public string TipoAuto { get; set; }
            public short Anio { get; set; }
            public int Costo { get; set; }
            public bool Pagado { get; set; }
            public bool Financiamiento { get; set; }
            public byte Meses { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosAutos
        {
            public string Expediente { get; set; }
            public string Usuario { get; set; }
            public List<ParametrosAuto> Autos { get; set; }
        }
        public class ParametrosAuto
        {
            public string MarcaModelo { get; set; }
            public string TipoAuto { get; set; }
            public short Anio { get; set; }
            public int Costo { get; set; }
            public bool Pagado { get; set; }
            public bool Financiamiento { get; set; }
            public byte Meses { get; set; }
        }

        public class ParametrosUpdateBienesInversiones
        {
            public long Id { get; set; }
            public string Tipo { get; set; }
            public int Valor { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosBienesInversiones
        {
            public string Expediente { get; set; }
            public string Usuario { get; set; }
            public List<ParametrosBienInversion> BienesInversiones { get; set; }
        }
        public class ParametrosBienInversion
        {
            public string Tipo { get; set; }
            public int Valor { get; set; }
        }
        public class ParametrosConyugues
        {
            public string Expediente { get; set; }
            public string Nombre { get; set; }
            public bool Vive { get; set; }
            public string Ocupacion { get; set; }
            public bool Trabaja { get; set; }
            public string Puesto { get; set; }
            public string Empresa { get; set; }
            public int IngresoNeto { get; set; }
            public string Usuario { get; set; }
        }
        public class ParametrosDocumentos
        {
            public string Expediente { get; set; }
            public string CartaCompPago { get; set; }
            public string CartaAdminProg { get; set; }
            public string CartaSolBeca { get; set; }
            public string CalifUltimoGrado { get; set; }
            public string CartaAsignaBeca { get; set; }
            public string CartaJefe { get; set; }
            public string PlanTrabajo { get; set; }
            public string PlanTrabajoVice { get; set; }
            public string Usuario { get; set; }

        }
        public class ParametrosEmpleos
        {
            public string Expediente { get; set; }
            public decimal IngresoNeto { get; set; }
            public string NombreEmpresa { get; set; }
            public string NaturalezaEmpresa { get; set; }
            public string Puesto { get; set; }
            public short Anios { get; set; }
            public string Usuario { get; set; }
        }
        public class ParametrosGastos
        {
            public string Expediente { get; set; }
            public decimal RentaHipoteca { get; set; }
            public decimal Servicios { get; set; }
            public decimal Educacion { get; set; }
            public decimal Medicos { get; set; }
            public decimal Alimentos { get; set; }
            public decimal Vestido { get; set; }
            public decimal Seguro { get; set; }
            public decimal Diversion { get; set; }
            public decimal Ahorro { get; set; }
            public decimal CreditosImpuestos { get; set; }
            public decimal Otros { get; set; }
            public string Usuario { get; set; }
        }
        public class ParametrosGrados
        {
            public string Expediente { get; set; }
            public string UltimoGrado { get; set; }
            public string EgresadoLic { get; set; }
            public decimal PromedioLic { get; set; }
            public bool TituladoLic { get; set; }
            public string EgresadoMas { get; set; }
            public decimal PromedioMas { get; set; }
            public bool TituladoMas { get; set; }
            public string EgresadoDoc { get; set; }
            public decimal PromedioDoc { get; set; }
            public bool TituladoDoc { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosUpdateHijo
        {
            public long Id { get; set; }
            public string Nombre { get; set; }
            public byte Edad { get; set; }
            public string Ocupacion { get; set; }
            public int Colegiatura { get; set; }
            public byte PorcenajeBeca { get; set; }
            public int Ingresos { get; set; }
            public string Usuario { get; set; }
        }

        public class ParametrosHijos
        {
            public string Expediente { get; set; }
            public string Usuario { get; set; }
            public List<ParametrosHijo> Hijos { get; set; }
        }

        public class ParametrosHijo
        {
            public string Nombre { get; set; }
            public byte Edad { get; set; }
            public string Ocupacion { get; set; }
            public int Colegiatura { get;set; }
            public byte PorcenajeBeca { get; set; }
            public int Ingresos { get; set; }
        }
        public class ParametrosViviendas
        {
            public string Expediente { get; set; }
            public string TipoVivienda { get; set; }
            public short Terreno { get; set; }
            public short SuperficieConstruida { get; set; }
            public string Condiciones { get; set; }
            public int Valor { get; set; }
            public string Usuario { get; set; }

        }

        public class ParametrosExpediente {
            public string Expediente { get; set; }

        }
        public class ParametrosBorrar
        {
            public long Id { get; set; }
            public string Usuario { get; set; }
        }
    } 
}