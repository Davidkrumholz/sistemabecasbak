//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace becasPosgrado
{
    using System;
    using System.Collections.Generic;
    
    public partial class TP_Conyugues
    {
        public long ID { get; set; }
        public long ID_PROSPECTO { get; set; }
        public string NOMBRE { get; set; }
        public Nullable<bool> VIVE { get; set; }
        public string OCUPACION { get; set; }
        public Nullable<bool> TRABAJA { get; set; }
        public string PUESTO { get; set; }
        public string EMPRESA { get; set; }
        public Nullable<long> INGRESO_NETO { get; set; }
        public Nullable<bool> Activo { get; set; }
        public string Usuario { get; set; }
        public Nullable<System.DateTime> Fecha_Mod { get; set; }
        public Nullable<System.DateTime> Fecha_Crea { get; set; }
    }
}
